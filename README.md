# PingSensorCode

Code sharing for VL53L0X time of flight sensor.  
  
For Parallax PING Laser sensor code see under `/old_PING_Laser_sensor` directory.  

For RaspberryPi's running Debian Raspi OS - be sure to turn on I2c Bus via `sudo raspi-config` under Interface Options.  
  
Relies on python3-smbus package: `sudo apt install python3-smbus`  
Relies on the smbus2 package: `pip3 install smbus2`  
Relies on the RPi.GPIO package: `pip3 install RPi.GPIO`  
Relies on Paho MQTT package for message bus: `pip3 install paho-mqtt`   
Relies on the Flask webserver and associated packages: `pip3 install Flask`    
Relies on the VX53L0X.py file at: https://github.com/pimoroni/VL53L0X-python (note file included in repo)  

You **must** also make the VL53L0X_rasp_python.so file to include the i2c read and write functions:  

`pip3 install git+https://github.com/pimoroni/VL53L0X-python.git`  
  
To build on raspberry pi, first make sure you have the right tools and development libraries:  
`sudo apt-get install build-essential python-dev`  


Then use following commands to clone the repository and compile:  
```
cd your_git_directory  
git clone https://github.com/pimoroni/VL53L0X_rasp_python.git  
cd VL53L0X-python  
make  
```  

## NOTE: The path to the .so file is hard coded in the program in the VX53L0X.py file - edit path if not found  
## NOTE: The MQTT Server is hard coded and should be changed for your setup - there are 6 places to change this in ping.py and 1 in data_processor.py   
## NOTE: The Flask web server runs on port 9999, but you can edit this on the last line of the ui_v1_0.py program

You will need an installed and running MQTT server.   
The MQTT topic that the messages are sent on are "store43/tray1" or "store43/tray2". 
You will need to run the data_processor.py in a terminal window and leave it running. This program converts the message queue information to a JSON inventory file.   
You will need to run the ui_v1_0.py in a terminal window and leave it running. This program runs the web server on localhost:9999.   

You can use vl_single.py to test an individual sensor on 0x29 I2C address.
You can use vl_multi.py to test 3 sensors and addressing the sensors on the I2C bus.
