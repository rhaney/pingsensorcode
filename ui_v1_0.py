from flask import Flask, render_template, request, redirect
import json
import requests
import os.path
## import loe (call with loe.function_name())
from datetime import datetime


app = Flask(__name__)
app.debug = True


@app.route('/', methods=['GET'])
def root():

    # if POST set variables from form 
    if request.method == 'POST':
        return render_template('/inventory.html')
    return render_template('/index.html')

@app.route('/index', methods=['GET', 'POST'])
def index():
    # if POST set variables from form
    if request.method == 'POST':
        return render_template('/inventory.html')
    return render_template('/index.html')

@app.route('/inventory', methods=['GET', 'POST'])
def inventory():
    
    # if POST set variables 
    if request.method == 'POST':
        store_num = request.form['Store_Num']
    # Create Strings to insert from JSON file
    with open('inventory1.json') as json_data_file:
        json_object = json.load(json_data_file)
        print(json_object)
        tray_num = json_object["tray"]
        slot1_num = json_object["slot1"]
        slot2_num = json_object["slot2"]
        slot3_num = json_object["slot3"]
        slot4_num = json_object["slot4"]
        slot5_num = json_object["slot5"]             
    return render_template('/inventory.html', store_num=store_num, tray_num=tray_num, slot1_num=slot1_num, slot2_num=slot2_num, slot3_num=slot3_num, slot4_num=slot4_num, slot5_num=slot5_num)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9999)
