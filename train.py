#!/usr/bin/python3

import time
import datetime
import VL53L0X
import RPi.GPIO as GPIO
# from func_timeout import func_timeout, FunctionTimedOut

# # GPIO for Sensor 1 shutdown pin
sensor1_shutdown = 17
# GPIO for Sensor 2 shutdown pin
sensor2_shutdown = 27
# GPIO for Sensor 3 shutdown pin
sensor3_shutdown = 22
# GPIO for Sensor 4 shutdown pin
sensor4_shutdown = 23
# GPIO for Sensor 5 shutdown pin
sensor5_shutdown = 24

GPIO.setwarnings(False)

# Setup GPIO for shutdown pins on each VL53L0X
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor1_shutdown, GPIO.OUT)
GPIO.setup(sensor2_shutdown, GPIO.OUT)
GPIO.setup(sensor3_shutdown, GPIO.OUT)
GPIO.setup(sensor4_shutdown, GPIO.OUT)
GPIO.setup(sensor5_shutdown, GPIO.OUT)

# Set all shutdown pins low to turn off each VL53L0X
GPIO.output(sensor1_shutdown, GPIO.LOW)
GPIO.output(sensor2_shutdown, GPIO.LOW)
GPIO.output(sensor3_shutdown, GPIO.LOW)
GPIO.output(sensor4_shutdown, GPIO.LOW)
GPIO.output(sensor5_shutdown, GPIO.LOW)

# Keep all low for 500 ms or so to make sure they reset
time.sleep(0.50)

# Create one object per VL53L0X, passing the address to set
tof1 = VL53L0X.VL53L0X(address=0x31)
tof2 = VL53L0X.VL53L0X(address=0x32)
tof3 = VL53L0X.VL53L0X(address=0x33)
tof4 = VL53L0X.VL53L0X(address=0x34)
tof5 = VL53L0X.VL53L0X(address=0x35)

# Set shutdown pin high for the first VL53L0X then 
# call to start ranging 
GPIO.output(sensor1_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof1.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

# Set shutdown pin high for the second VL53L0X then 
# call to start ranging 
GPIO.output(sensor2_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof2.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

# Set shutdown pin high for the third VL53L0X then 
# call to start ranging 
GPIO.output(sensor3_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof3.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

# Set shutdown pin high for the fourth VL53L0X then 
# call to start ranging 
GPIO.output(sensor4_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof4.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

# Set shutdown pin high for the fifth VL53L0X then 
# call to start ranging 
GPIO.output(sensor5_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof5.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

print("setup Complete")

# Test timing for Accuracy mode (Good Accuracy mode = 33 ms & 1.2m range,
# Better Accuracy mode = 66 ms & 1.2m range, Best Accuracy mode = 200 ms 1.2m range
# Long Range mode (indoor,darker conditions) = 33 ms & 2m range
# High Speed Mode (decreased accuracy) = 20 ms & 1.2m range)
timing = tof1.get_timing()
if (timing < 20000):
    timing = 20000
print ("Timing %d ms" % (timing/1000))

def MakeAvg(pin):
    dist_list = []
    i = 1
    while i < 13:
        # wrapper to timeout if LaserPing misfires
        try:
            if pin == 1:
                distance = tof1.get_distance()
            if pin == 2:
                distance = tof2.get_distance()
            if pin == 3:
                distance = tof3.get_distance()
            if pin == 4:
                distance = tof4.get_distance()
            if pin == 5:
                distance = tof5.get_distance()    
        except Exception as e:
	        # Handle any exceptions that GetDistance() might raise here
            print('Exception Error in GetDistance Function' + str(e))
            distance = 1
        dist_list.append(distance)
        time.sleep(.2)
        i += 1
    # Sort List and remove both end values to clip outliers
    dist_list.sort()
    del dist_list[0]
    del dist_list[0]
    del dist_list[0]
    del dist_list[-1]
    del dist_list[-1]
    del dist_list[-1]
    # Average remaining list values
    avg_dist = round(sum(dist_list)/len(dist_list))
    return avg_dist

def MakeAvgAvg(pin):
    dist_list = []
    i = 1
    while i < 13:
        distance = MakeAvg(pin)
        dist_list.append(distance)
        i += 1
    # Sort List and remove both end values to clip outliers
    dist_list.sort()
    del dist_list[0]
    del dist_list[-1]
    # Average remaining list values
    avg_dist = round(sum(dist_list)/len(dist_list))
    return avg_dist

def main(args):
    dict_vars = vars(args)
    slot_num = dict_vars['slot']

    while True:
        distance = MakeAvgAvg(1)
        print("Tray1 average distance for slot ",slot_num," is ",distance," mm")
        distance = MakeAvgAvg(2)
        print("Tray2 average distance for slot ",slot_num," is ",distance," mm")
        distance = MakeAvgAvg(3)
        print("Tray3 average distance for slot ",slot_num," is ",distance," mm")
        distance = MakeAvgAvg(4)
        print("Tray4 average distance for slot ",slot_num," is ",distance," mm")
        distance = MakeAvgAvg(5)
        print("Tray5 average distance for slot ",slot_num," is ",distance," mm")
        print(".")
        time.sleep(1)

if __name__ == '__main__':    
    import argparse
  
    parser = argparse.ArgumentParser(description='Average Distance to Tray/Slot.')
    parser.add_argument('slot', help='Slot of Object')
    args, unknown = parser.parse_known_args()

    main(args)
