import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("store43/tray1")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    payload_str = msg.payload.decode("utf-8")
    # msg_str = msg.topic+" "+str(msg.payload)
    msg_str = str(payload_str)
    print(msg_str)
    with open('inventory1.json', 'w') as f:
        f.write(msg_str)
        f.close()
    # here we need to do our db record building and push
    # convert string to json?

    # build record
    # try - push record to db & error recover

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

# db client
# db connect

client.connect("10.225.27.50", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
