#!/usr/bin/python3

import time
import datetime
import VL53L0X
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt

# GPIO for Sensor 1 shutdown pin
sensor1_shutdown = 17
# GPIO for Sensor 2 shutdown pin
sensor2_shutdown = 27
# GPIO for Sensor 3 shutdown pin
sensor3_shutdown = 22
# GPIO for Sensor 4 shutdown pin
sensor4_shutdown = 23
# GPIO for Sensor 5 shutdown pin
sensor5_shutdown = 24

GPIO.setwarnings(False)

# Setup GPIO for shutdown pins on each VL53L0X
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor1_shutdown, GPIO.OUT)
GPIO.setup(sensor2_shutdown, GPIO.OUT)
GPIO.setup(sensor3_shutdown, GPIO.OUT)
GPIO.setup(sensor4_shutdown, GPIO.OUT)
GPIO.setup(sensor5_shutdown, GPIO.OUT)

# Set all shutdown pins low to turn off each VL53L0X
GPIO.output(sensor1_shutdown, GPIO.LOW)
GPIO.output(sensor2_shutdown, GPIO.LOW)
GPIO.output(sensor3_shutdown, GPIO.LOW)
GPIO.output(sensor4_shutdown, GPIO.LOW)
GPIO.output(sensor5_shutdown, GPIO.LOW)

# Keep all low for 500 ms or so to make sure they reset
time.sleep(0.50)

# Create one object per VL53L0X, passing the address to set
tof1 = VL53L0X.VL53L0X(address=0x31)
tof2 = VL53L0X.VL53L0X(address=0x32)
tof3 = VL53L0X.VL53L0X(address=0x33)
tof4 = VL53L0X.VL53L0X(address=0x34)
tof5 = VL53L0X.VL53L0X(address=0x35)

# Set up outbound message publisher
message_client = mqtt.Client()
message_client.connect('10.225.27.50', port=1883, keepalive=60)

print("Setup Complete")

# Test timing for Accuracy mode (Good Accuracy mode = 33 ms & 1.2m range,
# Better Accuracy mode = 66 ms & 1.2m range, Best Accuracy mode = 200 ms 1.2m range
# Long Range mode (indoor,darker conditions) = 33 ms & 2m range
# High Speed Mode (decreased accuracy) = 20 ms & 1.2m range)
GPIO.output(sensor1_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof1.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
timing = tof1.get_timing()
if (timing < 20000):
    timing = 20000
print ("Timing %d ms" % (timing/1000))
tof1.stop_ranging()

# List of trained sensor values for slot distances
tray1 = [373.5, 350.5, 320.5, 285, 252.5, 222, 189.5, 161.5, 136, 108.5, 84, 62.5]
tray2 = [362, 351, 327.5, 300, 275.5, 245, 214.5, 187.5, 159.5, 129.5, 104.5, 84]
tray3 = [424.5, 403.5, 374, 333, 281.5, 239, 203, 168, 141.5, 117, 90.5, 69.5]
tray4 = [362, 351, 327.5, 300, 275.5, 245, 214.5, 187.5, 159.5, 129.5, 104.5, 84]
tray5 = [424.5, 403.5, 374, 333, 281.5, 239, 203, 168, 141.5, 117, 90.5, 69.5]

def MakeAvg(pin):
    dist_list = []
    i = 1
    while i < 13:
        # Need error wrapper to timeout if sensor misfires
        try:
            if pin == 1:
                distance = tof1.get_distance()
            if pin == 2:
                distance = tof2.get_distance()
            if pin == 3:
                distance = tof3.get_distance()
            if pin == 4:
                distance = tof4.get_distance()
            if pin == 5:
                distance = tof5.get_distance()    
        except Exception as e:
	        # Handle any exceptions that GetDistance() might raise here
            print('Exception Error in GetDistance Function' + str(e))
            distance = 1
        dist_list.append(distance)
        time.sleep(.2)
        i += 1
    # Sort List and remove both end values to clip outliers
    dist_list.sort()
    del dist_list[0]
    del dist_list[0]
    del dist_list[0]
    del dist_list[-1]
    del dist_list[-1]
    del dist_list[-1]
    # Average remaining list values
    avg_dist = round(sum(dist_list)/len(dist_list))
    return avg_dist

def SlotLookup(tray_num, dist):
    # Used to lookup the slot from the trained list of values
    if tray_num == 1:
        for i in range(13):
            try:
                if dist > tray1[i]:
                    return (i)
            except:
                return (12)
        return(12)
    if tray_num == 2:
        for i in range(13):
            try:
                if dist > tray2[i]:
                    return (i)
            except:
                return (12)
        return(12)
    if tray_num == 3:
        for i in range(13):
            try:
                if dist > tray3[i]:
                    return (i)
            except:
                return (12)
        return(12)
    if tray_num == 4:
        for i in range(13):
            try:
                if dist > tray4[i]:
                    return (i)
            except:
                return (12)
        return(12)
    if tray_num == 5:
        for i in range(13):
            try:
                if dist > tray5[i]:
                    return (i)
            except:
                return (12)
        return(12)  


def main():
    # Declare global variables
    t1old_slot_num = 1
    t1counter = 0
    t2old_slot_num = 1
    t2counter = 0
    t3old_slot_num = 1
    t3counter = 0
    t4old_slot_num = 1
    t4counter = 0
    t5old_slot_num = 1
    t5counter = 0
    while True:
        ### Sensor 1
        GPIO.output(sensor1_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        tof1.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        distance = MakeAvg(1)
        if (distance <= 0):
            print ("%d - Error" % tof1.my_object_number)
        t1slot_num = SlotLookup(1, distance)
        tof1.stop_ranging()
        GPIO.output(sensor1_shutdown, GPIO.LOW)
        if t1slot_num == t1old_slot_num:
            t1counter = 0
        else:
            t1counter +=1
        if t1counter > 2:
            t1old_slot_num = t1slot_num
            t1counter = 0
            now = datetime.datetime.now()
            msg = '{"store": "43", "tray": "1", "slot1": "' + str(t1slot_num) + '", "slot2": "' + str(t2old_slot_num) + '", "slot3": "' + str(t3old_slot_num) + '", "slot4": "' + str(t4old_slot_num) + '", "slot5": "' + str(t5old_slot_num) + '", "timestamp": "' + now.strftime("%m%d%y %H:%M:%S") + '"}'
            print(msg)
            try:
                message_client.connect('10.225.27.50', port=1883, keepalive=60)
                message_client.publish("store43/tray1", payload=msg)
                message_client.disconnect()
            except Exception as e:
	            # Handle any exceptions that GetDistance() might raise here
                print('Exception Error in Message Bus Function' + str(e))

        ### Sensor 2
        GPIO.output(sensor2_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        tof2.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        distance = MakeAvg(2)
        if (distance <= 0):
            print ("%d - Error" % tof2.my_object_number)
        t2slot_num = SlotLookup(2, distance)
        tof2.stop_ranging()
        GPIO.output(sensor2_shutdown, GPIO.LOW)
        if t2slot_num == t2old_slot_num:
            t2counter = 0
        else:
            t2counter +=1
        if t2counter > 2:
            t2old_slot_num = t2slot_num
            t2counter = 0
            now = datetime.datetime.now()
            msg = '{"store": "43", "tray": "1", "slot1": "' + str(t1slot_num) + '", "slot2": "' + str(t2old_slot_num) + '", "slot3": "' + str(t3old_slot_num) + '", "slot4": "' + str(t4old_slot_num) + '", "slot5": "' + str(t5old_slot_num) + '", "timestamp": "' + now.strftime("%m%d%y %H:%M:%S") + '"}'
            print(msg)
            try:
                message_client.connect('10.225.27.50', port=1883, keepalive=60)
                message_client.publish("store43/tray1", payload=msg)
                message_client.disconnect()
            except Exception as e:
	            # Handle any exceptions that GetDistance() might raise here
                print('Exception Error in Message Bus Function' + str(e))
            
        ### Sensor 3
        GPIO.output(sensor3_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        tof3.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        distance = MakeAvg(3)
        if (distance <= 0):
            print ("%d - Error" % tof3.my_object_number)
        t3slot_num = SlotLookup(3, distance)
        tof3.stop_ranging()
        GPIO.output(sensor3_shutdown, GPIO.LOW)
        if t3slot_num == t3old_slot_num:
            t3counter = 0
        else:
            t3counter +=1
        if t3counter > 2:
            t3old_slot_num = t3slot_num
            t3counter = 0
            now = datetime.datetime.now()
            msg = '{"store": "43", "tray": "1", "slot1": "' + str(t1slot_num) + '", "slot2": "' + str(t2old_slot_num) + '", "slot3": "' + str(t3old_slot_num) + '", "slot4": "' + str(t4old_slot_num) + '", "slot5": "' + str(t5old_slot_num) + '", "timestamp": "' + now.strftime("%m%d%y %H:%M:%S") + '"}'
            print(msg)
            try:
                message_client.connect('10.225.27.50', port=1883, keepalive=60)
                message_client.publish("store43/tray1", payload=msg)
                message_client.disconnect()
            except Exception as e:
	            # Handle any exceptions that GetDistance() might raise here
                print('Exception Error in Message Bus Function' + str(e))

        ### Sensor 4        
        GPIO.output(sensor4_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        tof4.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        distance = MakeAvg(4)
        if (distance <= 0):
            print ("%d - Error" % tof4.my_object_number)
        t4slot_num = SlotLookup(4, distance)
        tof4.stop_ranging()
        GPIO.output(sensor4_shutdown, GPIO.LOW)
        if t4slot_num == t4old_slot_num:
            t4counter = 0
        else:
            t4counter +=1
        if t4counter > 2:
            t4old_slot_num = t4slot_num
            t4counter = 0
            now = datetime.datetime.now()
            msg = '{"store": "43", "tray": "1", "slot1": "' + str(t1slot_num) + '", "slot2": "' + str(t2old_slot_num) + '", "slot3": "' + str(t3old_slot_num) + '", "slot4": "' + str(t4old_slot_num) + '", "slot5": "' + str(t5old_slot_num) + '", "timestamp": "' + now.strftime("%m%d%y %H:%M:%S") + '"}'
            print(msg)
            try:
                message_client.connect('10.225.27.50', port=1883, keepalive=60)
                message_client.publish("store43/tray1", payload=msg)
                message_client.disconnect()
            except Exception as e:
	            # Handle any exceptions that GetDistance() might raise here
                print('Exception Error in Message Bus Function' + str(e))

        ### Sensor 5
        GPIO.output(sensor5_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        tof5.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        distance = MakeAvg(5)
        if (distance <= 0):
            print ("%d - Error" % tof5.my_object_number)
        t5slot_num = SlotLookup(5, distance)
        tof5.stop_ranging()
        GPIO.output(sensor5_shutdown, GPIO.LOW)
        if t5slot_num == t5old_slot_num:
            t5counter = 0
        else:
            t5counter +=1
        if t5counter > 2:
            t5old_slot_num = t5slot_num
            t5counter = 0
            now = datetime.datetime.now()
            msg = '{"store": "43", "tray": "1", "slot1": "' + str(t1slot_num) + '", "slot2": "' + str(t2old_slot_num) + '", "slot3": "' + str(t3old_slot_num) + '", "slot4": "' + str(t4old_slot_num) + '", "slot5": "' + str(t5old_slot_num) + '", "timestamp": "' + now.strftime("%m%d%y %H:%M:%S") + '"}'
            print(msg)
            try:
                message_client.connect('10.225.27.50', port=1883, keepalive=60)
                message_client.publish("store43/tray1", payload=msg)
                message_client.disconnect()
            except Exception as e:
	            # Handle any exceptions that GetDistance() might raise here
                print('Exception Error in Message Bus Function' + str(e))

        # Troubleshooting Code
        print("*****************")
        print("slot1_now: " + str(t1slot_num) + ", slot2_now: " + str(t2slot_num) + ", slot3_now: " + str(t3slot_num) + ", slot4_now: " + str(t4slot_num) + ", slot5_now: " + str(t5slot_num))    
        print("slot1_old: " + str(t1old_slot_num) + ", slot2_old: " + str(t2old_slot_num) + ", slot3_old: " + str(t3old_slot_num) + ", slot4_old: " + str(t4old_slot_num) + ", slot5_old: " + str(t5old_slot_num))
        print("counter1: " + str(t1counter) + ", counter2: " + str(t2counter) + ", counter3: " + str(t3counter) + ", counter4: " + str(t4counter) + ", counter5: " + str(t5counter))
        print("*****************")


if __name__ == '__main__':

    main()
