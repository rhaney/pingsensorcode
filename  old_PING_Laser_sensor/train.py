#!/usr/bin/python3

import time
import RPi.GPIO as GPIO
from func_timeout import func_timeout, FunctionTimedOut

# Use board based pin numbering
GPIO.setmode(GPIO.BCM)


def ReadDistance(pin):
    # Set pin to OUT & LOW to begin trigger
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)
    time.sleep(0.000002)

    # send trigger signal (set pin to HIGH)
    GPIO.output(pin, 1)
    # Pulse
    time.sleep(0.000007)
    # Set pin to LOW & INPUT to listen for response
    GPIO.output(pin, 0)
    GPIO.setup(pin, GPIO.IN)
    # Set Start Time
    while GPIO.input(pin)==0:
      starttime=time.time()
    # Wait for Response and Set End Time
    while GPIO.input(pin)==1:
      endtime=time.time()
    # Exception block to catch no End Time
    try:
       duration=endtime-starttime
    except:
        # Create False Value (Too High)
        duration = .1
    # Distance is defined as time/2 (there and back) * speed of sound at 34000 cm/s
    distance=round(duration*150000, 1)
    return distance

def MakeAvg(pin):
    dist_list = []
    i = 1
    while i < 13:
        # wrapper to timeout if LaserPing misfires
        try:
            # Catch hang error if time outside of distance bounds
            distance = func_timeout(2, ReadDistance, args=(pin,))
        except FunctionTimedOut:
            # Set impossible distance (Too Low)
	        distance = 1
        except Exception as e:
	        # Handle any exceptions that ReadDistance() might raise here
            print('Exception Error in ReadDistance Function' + str(e))
            distance = 1
        dist_list.append(distance)
        time.sleep(.2)
        i += 1
    # Sort List and remove both end values to clip outliers
    dist_list.sort()
    del dist_list[0]
    del dist_list[0]
    del dist_list[0]
    del dist_list[-1]
    del dist_list[-1]
    del dist_list[-1]
    # Average remaining list values
    avg_dist = round(sum(dist_list)/len(dist_list))
    return avg_dist

def MakeAvgAvg(pin):
    dist_list = []
    i = 1
    while i < 13:
        distance = MakeAvg(pin)
        dist_list.append(distance)
        i += 1
    # Sort List and remove both end values to clip outliers
    dist_list.sort()
    del dist_list[0]
    del dist_list[-1]
    # Average remaining list values
    avg_dist = round(sum(dist_list)/len(dist_list))
    return avg_dist

def main(args):
    dict_vars = vars(args)
    slot_num = dict_vars['slot']

    while True:
        distance = MakeAvgAvg(23)
        print("Tray1 average distance for slot ",slot_num," is ",distance," mm")
        distance = MakeAvgAvg(17)
        print("Tray2 average distance for slot ",slot_num," is ",distance," mm")
        distance = MakeAvgAvg(27)
        print("Tray3 average distance for slot ",slot_num," is ",distance," mm")
        print(".")
        time.sleep(1)

if __name__ == '__main__':    
    import argparse
  
    parser = argparse.ArgumentParser(description='Average Distance to Tray/Slot.')
    parser.add_argument('slot', help='Slot of Object')
    args, unknown = parser.parse_known_args()

    main(args)
