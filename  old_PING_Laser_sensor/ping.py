#!/usr/bin/python3

import time
import datetime
import RPi.GPIO as GPIO
from func_timeout import func_timeout, FunctionTimedOut

# Use board based pin numbering
GPIO.setmode(GPIO.BCM)

# Create list of trained sensor values for slot distances
tray1 = [354.5, 324, 281.5, 242.5, 211, 183, 159.5, 140.5, 116]
tray2 = [253, 230, 202, 170.5, 140.5, 115.5, 83.5, 56.5, 33.5]
tray3 = [271, 233.5, 192.5, 152, 123, 103.5, 76.5, 48.5, 30.5]

def ReadDistance(pin):
    # Set pin to OUT & LOW to begin trigger
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)
    time.sleep(0.000002)

    # send trigger signal (set pin to HIGH)
    GPIO.output(pin, 1)
    # Pulse
    time.sleep(0.000007)
    # Set pin to LOW & INPUT to listen for response
    GPIO.output(pin, 0)
    GPIO.setup(pin, GPIO.IN)
    # Set Start Time
    while GPIO.input(pin)==0:
      starttime=time.time()
    # Wait for Response and Set End Time
    while GPIO.input(pin)==1:
      endtime=time.time()
    # Exception block to catch no End Time
    try:
       duration=endtime-starttime
    except:
        # Create False Value (Too High)
        duration = .1
        print('Endtime Error')
    # Distance is defined as time/2 (there and back) * speed of sound at 34000 cm/s
    distance=round(duration*150000, 1)
    return distance

def MakeAvg(pin):
    dist_list = []
    i = 1
    while i < 13:
        # wrapper to timeout if LaserPing misfires
        try:
            # Catch hang error if time outside of distance bounds
            distance = func_timeout(2, ReadDistance, args=(pin,))
        except FunctionTimedOut:
            # Set impossible distance (Too Low)
            # print('Timeout Error')
	        distance = 1
        except Exception as e:
	        # Handle any exceptions that ReadDistance() might raise here
            print('Exception Error in ReadDistance Function' + str(e))
            distance = 1
        dist_list.append(distance)
        time.sleep(.2)
        i += 1
    # Sort List and remove both end values to clip outliers
    dist_list.sort()
    del dist_list[0]
    del dist_list[0]
    del dist_list[0]
    del dist_list[-1]
    del dist_list[-1]
    del dist_list[-1]
    # Average remaining list values
    avg_dist = round(sum(dist_list)/len(dist_list))
    return avg_dist

def SlotLookup(tray_num, dist):
    # Used to lookup the slot from the trained list of values
    if tray_num == 1:
        for i in range(10):
            try:
                if dist > tray1[i]:
                    return (i)
            except:
                return (10)
        return(10)
    if tray_num == 2:
        for i in range(10):
            try:
                if dist > tray2[i]:
                    return (i)
            except:
                return (10)
        return(10)
    if tray_num == 3:
        for i in range(10):
            try:
                if dist > tray3[i]:
                    return (i)
            except:
                return (10)
        return(10)

def main():

    while True:
        distance = MakeAvg(23)
        t1slot_num = SlotLookup(1, distance)
        distance = MakeAvg(17)
        t2slot_num = SlotLookup(2, distance)
        distance = MakeAvg(27)
        t3slot_num = SlotLookup(3, distance)
        print(datetime.datetime.now())
        print("Tray1: " + str(t1slot_num) + " | Tray2: " + str(t2slot_num) + " | Tray3: " + str(t3slot_num))
        time.sleep(1)

if __name__ == '__main__':

    main()
